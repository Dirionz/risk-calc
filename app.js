#!/usr/bin/env node
'use strict' 

const program = require('commander')
const riskcalc = require("./lib/riskcalc")

var cmdValue = undefined

program
    .version('0.0.1')

//function calculate(buyprice, stoploss, targetprice, courtage, kapital, risk) {
program
    .command("calculate")
    .alias('calc')
    .option('-b, --buyprice <n>', 'Specify buyprice', parseInt)
    .option('-s, --stoploss <n>', 'Specify stoploss', parseInt)
    .option('-t, --targetprice <n>', 'Specify targetprice', parseInt)
    .option('-c, --courtage <n>', 'Specify courtage', parseInt)
    .option('-k, --kapital <n>', 'Specify kapital', parseInt)
    .option('-r, --risk <n>', 'Specify risk', parseInt)
    .action(function (cmd) {
        if (cmd.buyprice && cmd.stoploss && cmd.targetprice && cmd.courtage 
            && cmd.kapital && cmd.risk) {
            cmdValue = cmd;
            var result = riskcalc.calculate(cmd.buyprice, cmd.stoploss, cmd.targetprice, 
                                                cmd.courtage, cmd.kapital, cmd.risk)
            console.log(result)
        } else {
            console.error('something went wrong!');
            process.exit(1);
        }
    });

program.parse(process.argv);

if(!process.argv.slice(2).length) {
    program.outputHelp()
} else if (typeof cmdValue === 'undefined') {
   console.error('no command given!');
   process.exit(1);
}
//console.log('command:', cmdValue);
