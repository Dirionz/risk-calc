'use strict'

function calculate(buyprice, stoploss, targetprice, courtage, kapital, risk) {
    if (buyprice && stoploss && targetprice && kapital && risk) {
        var numberOfShares = Math.abs(Math.round(((kapital * (risk / 100)) - (2 * courtage)) / (buyprice - stoploss)))
        var possibleEarning = (Math.abs(targetprice - buyprice) * numberOfShares) - (2*courtage)
        var possibleLoss = -Math.abs((numberOfShares * stoploss) - (numberOfShares * buyprice)) - (courtage*2)
        //var sum = numberOfShares * buyprice + (courtage * 2);
        var sum = numberOfShares * buyprice + (courtage);
        
        return {
            "numberOfShares": numberOfShares,
            "possibleEarning": possibleEarning,
            "possibleLoss": possibleLoss,
            "sum": sum
        }

    }
}

module.exports = {
    calculate
}

