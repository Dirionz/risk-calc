'use strict' 

const riskcalc = require('../riskcalc');
const expect = require('chai').expect

describe('riskcalc', () => {
    it('should export calculate function', () => {
        expect(riskcalc.calculate).to.be.a('function')
    })
    describe('"calculate"', () => {
        it('should include keys', () => {
            expect(riskcalc.calculate(10, 5, 20, 0, 100000, 1)).to.have.all.keys([
                'numberOfShares', 'possibleEarning', 'possibleLoss', 'sum'
            ])
        })
        it('should calculate long correctly', () => {
            var result = riskcalc.calculate(10, 5, 20, 1, 100000, 1)
            //console.log(result)
            expect(result.numberOfShares).to.be.equals(200)
            expect(result.possibleEarning).to.be.equals(1998)
            expect(result.possibleLoss).to.be.equals(-1002)
            expect(result.sum).to.be.equals(2001)
        })
        it('should calculate short correctly', () => {
            var result = riskcalc.calculate(20, 25, 10, 1, 100000, 1)
            //console.log(result)
            expect(result.numberOfShares).to.be.equals(200)
            expect(result.possibleEarning).to.be.equals(1998)
            expect(result.possibleLoss).to.be.equals(-1002)
            expect(result.sum).to.be.equals(4001)
        })
    })
})

